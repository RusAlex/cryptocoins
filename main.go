package main

import (
	"cryptobot/app"
	"cryptobot/conf"
	"fmt"
	"os"
	"time"

	"cryptobot/controller"
	"cryptobot/services/rates"
	"cryptobot/services/robot2"
	"cryptobot/services/trading"

	"net/http"
	_ "net/http/pprof"

	"github.com/jinzhu/gorm"
	migrate "github.com/rubenv/sql-migrate"
	"github.com/sirupsen/logrus"
)

func main() {
	if len(os.Args) < 2 {
		fmt.Println("Invalid path to config file.")
		fmt.Println("Please follow next example:")
		fmt.Println("go run main.go ./conf/config.json")
		os.Exit(-1)
	}
	config := conf.FromFile(os.Args[1])

	if config.Debug.Log {
		logrus.SetLevel(logrus.DebugLevel)
	}
	db, err := initDB(config)
	for err != nil {
		logrus.WithError(err).Error("Failed init connection to database. Trying again after ten seconds")
		time.Sleep(time.Second * 10)
		db, err = initDB(config)
	}
	application := app.New(db)

	rateService := rates.New(db, config.Rates)
	tradingService := trading.New(application, rateService, config.Leverages, config.MinBalance, config.FixFactor)
	robots := robot2.New(tradingService.NewBatchRobotTradeCh, config.Robot, rateService, application)
	c := controller.New(config, rateService, application, tradingService, robots)

	go rateService.Start()
	go tradingService.Start()
	go tradingService.BatchTradesHandler()
	go robots.Start()
	go http.ListenAndServe(":8080", nil)
	logrus.Fatal(c.Start())
}

func initDB(conf conf.Main) (*gorm.DB, error) {
	db, err := gorm.Open("mysql", conf.DbConnURL())
	if err != nil {
		return nil, err
	}
	if err := db.DB().Ping(); err != nil {
		return nil, err
	}
	if conf.Debug.DB {
		db = db.Debug()
	}
	migrations := &migrate.FileMigrationSource{
		Dir: "db/migrations",
	}
	if _, err := migrate.Exec(db.DB(), "mysql", migrations, migrate.Up); err != nil {
		return nil, err
	}
	db.DB().SetMaxOpenConns(100)
	return db, nil
}
