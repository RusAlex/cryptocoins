#!/bin/bash -e

cat > ./conf/config.json << EOF
{
  "port": "80",
  "maxTradesPerDay": 0,
  "maxMarginPerDay": 0,
  "Auth": {
    "accessToken": "5G8AXoY8ASASm943ZQb9iDmp8EWEVuvB",
    "authTokenTtlSeconds": 2419200,
    "jwtSecret": "N7QCg3fz89YmtsbMzSkMuMDsmuJxZD32",
    "jwtTokenTtlMinutes": 10080
  },
  "rates": {
    "buyingMinPercents": 1,
    "buyingMaxPercents": 1.001,
    "sellingMinPercents": 1,
    "sellingMaxPercents": 1.001,
    "tableUpdateIntervalSeconds": 600,
    "demoRatesUpdateIntervalMinSeconds": 60,
    "demoRatesUpdateIntervalMaxSeconds": 180,
    "realRatesUpdateIntervalMinSeconds": 300,
    "realRatesUpdateIntervalMaxSeconds": 420
  },
  "database": {
    "name": "${CRYPTOBOT_DB_NAME}",
    "user": "${CRYPTOBOT_DB_USER}",
    "host": "${CRYPTOBOT_DB_HOST}",
    "port": "3306",
    "password": "${CRYPTOBOT_DB_PASSWORD}"
  },
  "frontEnd": {
    "apiTimeout": 5000,
    "logoutRedirectPath": "https://${DOMAIN}/logout/",
    "depositRedirectPath": "https://${DOMAIN}/my-account/invest/",
    "firstLinkRedirectPath": "https://${DOMAIN}/my-account/",
    "secondLinkRedirectPath": "https://${DOMAIN}/my-account/invest/",
    "firstLinkName": "Account",
    "secondLinkName": "Invest",
    "invalidJwtRedirectPath": "https://${DOMAIN}"
  },
  "demoBalance": 0.5,
  "minBalance": 0.0005,
  "leverages": [
    {
      "minBalance": 0.0,
      "leverage": 10,
      "profitPerDay": 0.25,
      "dailyTradeBoundLower": -0.003,
      "dailyTradeBoundUpper": 0.003,
      "resultTradeBoundLower": 0.002,
      "resultTradeBoundUpper": 0.003
    },
    {
      "minBalance": 0.036624,
      "leverage": 20,
      "profitPerDay": 0.5,
      "dailyTradeBoundLower": -0.0055,
      "dailyTradeBoundUpper": 0.0055,
      "resultTradeBoundLower": 0.0045,
      "resultTradeBoundUpper": 0.0055
    },
    {
      "minBalance": 0.182174,
      "leverage": 30,
      "profitPerDay": 1,
      "dailyTradeBoundLower": -0.0105,
      "dailyTradeBoundUpper": 0.0105,
      "resultTradeBoundLower": 0.0095,
      "resultTradeBoundUpper": 0.0105
    },
    {
      "minBalance": 0.364348,
      "leverage": 40,
      "profitPerDay": 1.5,
      "dailyTradeBoundLower": -0.0155,
      "dailyTradeBoundUpper": 0.0165,
      "resultTradeBoundLower": 0.0145,
      "resultTradeBoundUpper": 0.0165
    }
  ],
  "debug": {
    "log": true,
    "db": false
  },
  "robot": {
    "timeoutAfterNewTradeS": 10,
    "timeToExecutingTradeMS": 1600
  },
  "fixFactor": {
    "min": 0.00001,
    "max": 1
  },
  "security": {
    "allowed_admin_ip": [
      "172.18.0.0/24", 
      "46.28.204.34", 
      "46.28.207.150", 
      "95.183.55.80",
      "181.135.245.116"
    ]
  }
}
EOF

go run main.go ./conf/config.json
